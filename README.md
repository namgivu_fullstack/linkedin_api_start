--- goal 
able to call linkedin api to create new post

--- .
gg linkedin api "get started" let user integrate their linkedin and create new linkedin post
gg linkedin api ~better ~official ~tutorial

need to have linkedin app to start using linkedin api
https://developer.linkedin.com/ -> go for Create App https://www.linkedin.com/developers/apps/new?src=or-search&veh=www.google.com%7Cre-other
will require creating a Linkedin Page, Company logo

invite engineer to the app 
get invited, log in w/ invited account, go to My Apps
https://www.linkedin.com/developers/apps/212526808
go to Auth tab to get
Client ID
Client Secret

here, we looking for OAuth where we will have members consent to let us create new linked post on their behalf
https://docs.microsoft.com/linkedin/shared/authentication/authentication
and from there, we arrive at the detail guide about 3-legged OAuth [view](https://learn.microsoft.com/en-us/linkedin/shared/authentication/authorization-code-flow?source=recommendations&tabs=HTTPS1)

![Authorization Code Flow](linkedin-oauth-codeflow.png)

from the codeflow, 
get api res <- req api w/ access token
            <- clientid+clientsecret + authorization code fr member's login
            <- member login w/ their username+password and consent
            <- login web ui to call to /oauth/authorization
               ref https://learn.microsoft.com/en-us/linkedin/shared/authentication/authorization-code-flow?source=recommendations&tabs=HTTPS1#step-2-request-an-authorization-code
               > to request authorization code, must direct member's webbrowser to `LinkedIn's OAuth 2.0 authorization page`, where member will either accept or deny our permission request

GET https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id={your_client_id}&redirect_uri={your_callback_url}&state=somehardtoguess&scope=r_liteprofile%20r_emailaddress%20w_member_social
GET https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id={your_client_id}&redirect_uri={your_callback_url}&state=somehardtoguess&scope=profile%email%w_member_social
GET      ://                                        response_type=     client_id=                 redirect_uri=                    state=                scope=

during the process, linkedin will require redirect_uri to send back result
all those uri need to be added to linkedin developer app


--- linkedin api endpiont doc 
sign in to your linkedin developer app
-> go to Product tab
-> at product `Share on LinkedIn` go to `View endpoints`
                                  go to `View doc` ref https://learn.microsoft.com/en-us/linkedin/consumer/integrations/self-serve/share-on-linkedin

--- create new post
ref https://learn.microsoft.com/en-us/linkedin/consumer/integrations/self-serve/share-on-linkedin#creating-a-share-on-linkedin
no sample code / git repo for easy handson for engineer/developer

a git repo w/ sample python code
ref https://github.com/gutsytechster/linkedin-post
```
steps
Install the requirements using pip as pip install -r requirements.txt.
Create an app on `LinkedIn's developers account` here and follow the guide here to get the access token. 

After getting `access token`, send a GET request defined here to get the Person URN from the response.

Create a .env file and write the follwing content into it
ACCESS_TOKEN="<your access token>"
URN="<your urn>"
```
