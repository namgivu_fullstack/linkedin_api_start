from flask import Flask, json
#
import os
from dotenv import load_dotenv
#
from flask import request
import requests
#
from pathlib import Path


load_dotenv()
client_id     = os.environ.get('LINKEDINAPP_CLIENT_ID')     ; assert client_id, 'Envvar LINKEDINAPP_CLIENT_ID is required'
client_secret = os.environ.get('LINKEDINAPP_CLIENT_SECRET') ; assert client_id, 'Envvar LINKEDINAPP_CLIENT_SECRET is required'

app = Flask(__name__)


@app.route('/')
def index():
  # linkedin oauth  ref https://learn.microsoft.com/en-us/linkedin/shared/authentication/authorization-code-flow?source=recommendations&tabs=HTTPS1#step-2-request-an-authorization-code
  login_w_linkedin_link__echoparam      = f'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id={client_id}&state=somehardtoguess&scope=profile%20openid%20email%20w_member_social&redirect_uri=http://localhost:5000/get_linkedinapi_accesstoken__echoparam'
  login_w_linkedin_link__getaccesstoken = f'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id={client_id}&state=somehardtoguess&scope=profile%20openid%20email%20w_member_social&redirect_uri=http://localhost:5000/get_linkedinapi_accesstoken'
  #                                              ://                                        response_type=     client_id=            state=                scope=                                           redirect_uri=

  return f'''
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
  <ul>
    <li> <a href="{login_w_linkedin_link__echoparam}">      Log in w/ Linkedin</a> echoparam</li>
    <li> <a href="{login_w_linkedin_link__getaccesstoken}" >Log in w/ Linkedin</a> get accesstoken</li>
  </ul>
</body>
</html>  
  '''


@app.route('/get_linkedinapi_accesstoken__echoparam')
def get_linkedinapi_accesstoken__echoparam():
  # get Flask req param+body  ref https://stackoverflow.com/a/16664376/248616
  return json.dumps({
    'args': request.args,
    'form': request.form,
  })
'''
web address at :login_w_linkedin aka linkedin oauth/SSO request will return this 
so :code is the :authorization_code to proceed next step to get api access token
NOTE forsomereason, this :authorization_code cannot be stored in db and reuse to request :access_token bw. diff. webbrowser session
{
  "args": {
    "code": "AQTXpx7CEgADCjxGDs-2qFZJvMwq55oFkbC5wtBMQ41T4916LS7TcJKPpH14dBNUDnf8Qcki1vjVWzx8G26TteFslra9pOc3W9vUpqFJhBjysK7WMiYHmdfVxe9cfqkByadnozUcJ-QXOobkeUF9RhML-nKjnKzQ9H6U2bAhbm3diqhQKma39C2RAHtpgdtYx5nxBp-5yclB_UEmsNw",
    "state": "somehardtoguess"
  },
  "form": {
    
  }
}  
'''


@app.route('/get_linkedinapi_accesstoken')
def get_linkedinapi_accesstoken():
  '''
  member's authorized :code -> get api :accesstoken
  ref https://learn.microsoft.com/en-us/linkedin/shared/authentication/authorization-code-flow?source=recommendations&tabs=HTTPS1#step-3-exchange-authorization-code-for-an-access-token

  POST  https://www.linkedin.com/oauth/v2/accessToken
  Content-Type: application/x-www-form-urlencoded
  grant_type=authorization_code
  code={authorization_code_from_step2_response}
  client_id={your_client_id}
  client_secret={your_client_secret}
  redirect_uri={your_callback_url}
  '''
  member_authorized_code = request.args.get('code')
  assert member_authorized_code, 'Something not right, we MUST receive :code from :linkedin/oauth/v2/authorization'

  res = requests.post(
    'https://www.linkedin.com/oauth/v2/accessToken',
    headers={'Content-Type': 'application/x-www-form-urlencoded'},  #TODO do we need this?
    data={
      'grant_type'    : 'authorization_code',
      'code'          : member_authorized_code,
      'client_id'     : client_id,
      'client_secret' : client_secret,

      #redirect_uri   : this MUST BE the same as previous step ie get member consent :authorization_code
      'redirect_uri'  : 'http://localhost:5000/get_linkedinapi_accesstoken',
    },
  )

  access_token = res.json().get('access_token')

  # save to file :access_token ie res.json().get(access_token)
  THIS_DIR = Path(__file__).parent
  print(THIS_DIR)
  print(res.json())
  f = open(f'{THIS_DIR}/access_token.txt', 'w')
  f.write(access_token)
  f.close()

  return res.text
'''
sample success res
// 20230906144422
// http://localhost:5000/get_linkedinapi_accesstoken?code=AQS8ImRYLLm7Ncqri5hiOD7cl3F0ZEVRHOsRaoCrKssPRuuDAhTMdKjx0vIe8WDMN1CjvmKL0q3UhSShZDDkurTCXfG7QeNV1k9eGsbn4lJJI_HDV5ECySwtiz2_WC772nFt4SIF6Gg-Fm-_TksHmI99W49rl7V3XHbUt5ntgc-fn073Pxo9rbk9khFkbpRtkNmoxv7IMU5pB7X-4vE&state=somehardtoguess

{
  "access_token": "AQWOCkRhR4k2fO_Xz8L2k7HQT2784m04bIU1RYky02obVFiS_-prq3-j7uCmYzdDkoSf9_unDQc9DRvy4KTXirOpLKX-hRrLjwbb6LzSEV02Sw_oHqZIGZzrO0jLZij1xXMfdVAM8gGEOt55Vm3SCwjbKDLhOmmu-SS0eCR9ngzr157dChMlTg_Ek3s6-TrRkPwVFpiOG6fbTZYtQO1GTzo4GSgUFCpTw7fGT4ikxBoxxQjTGOANSdmgkWH6CWIrhNu8zudMsgzY_sQBpn79NC4ATwfwjMGKh3F3QdUHxirT25aPROTFFB5GIPkwQXlTjim1VFiDp_OOQoyMFy57RgMdBaZrkA",
  "expires_in": 5183999,
  "scope": "email,profile,w_member_social"
}
'''
