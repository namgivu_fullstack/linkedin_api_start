from service.linkedin_service import get__v2_userinfo

res = get__v2_userinfo()

print(f'''
{res.status_code=}
{res.text=}
{res.json()=}
''')

print(res.json() )
'''
{ 'sub': 'Wv4lc3_GsF', 'email_verified': True, 
  'name'        : 'Nam G. VU',  'locale': {'country': 'US', 'language': 'en'}, 
  'given_name'  : 'Nam',        'family_name': 'G. VU', 
  'email'       : 'namgivu@gmail.com', 
  'picture'     : 'https://media.licdn.com/dms/image/C4D03AQGqJ05p-RpJwA/profile-displayphoto-shrink_100_100/0/1516929279077?e=1699488000&v=beta&t=VzTfaqRdGnieJT4tJLi9e6CUITWh1ThT0YrZDPfhOdA'
}
'''
