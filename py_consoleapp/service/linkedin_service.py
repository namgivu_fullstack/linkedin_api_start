from dotenv import load_dotenv
import os
#
import requests
#
import datetime


load_dotenv()
v = os.environ.get('LINKEDIN_API__ACCESS_TOKEN') ; assert v, 'Envvar LINKEDIN_API__ACCESS_TOKEN is required' ; LINKEDIN_API__ACCESS_TOKEN = v


def get__v2_me():
  """
  ref https://learn.microsoft.com/en-us/linkedin/shared/authentication/authorization-code-flow?source=recommendations&tabs=HTTPS1#step-4-make-authenticated-requests
  curl -X GET https://api.linkedin.com/v2/me  -H 'Authorization: Bearer {INSERT_TOKEN}'
  """
  res = requests.get(  #CAUTION GET /v2/me not POST here  or will get err > Resource me does not exist  ref https://stackoverflow.com/a/69411120/248616
    'https://api.linkedin.com/v2/me',
    headers={'Authorization': f'Bearer {LINKEDIN_API__ACCESS_TOKEN}'},
  )
  return res


def get__v2_userinfo():
  """
  see the endpoint in Auth tab @ linkedin developer app
  """
  res = requests.get(  #CAUTION GET not POST here
    'https://api.linkedin.com/v2/userinfo',
    headers={'Authorization': f'Bearer {LINKEDIN_API__ACCESS_TOKEN}'},
  )
  return res


def post__v2_ugcPosts():
  """
  ref https://github.com/gutsytechster/linkedin-post.git
  """
  #TODO refresh if near expired :access_token  - hint00 @ we have :expire_id returned when requesting :access_token  - hint01 ref https://learn.microsoft.com/en-us/linkedin/shared/authentication/programmatic-refresh-tokens?context=linkedin%2Fcontext <- https://learn.microsoft.com/en-us/linkedin/shared/authentication/authorization-code-flow?tabs=HTTPS1#step-5-refresh-access-token

  def get_urn_author():
    """get URN is the :sub returned from /v2/userinfo"""
    res           = get__v2_userinfo()
    person_urn_id = res.json().get('sub')
    assert person_urn_id, 'Something not right; cannot get urn fr /v2/userinfo'

    urn_author = f'urn:li:person:{person_urn_id}'
    #          =   urn:{namespace}:{entityType}:{id}  ref https://learn.microsoft.com/en-us/linkedin/shared/api-guide/concepts/urns#urns
    return urn_author
  urn_author = get_urn_author()

  post_content = f'''
[TESTING] test POST /v2/ugcPosts
view code at https://gitlab.com/namgivu_fullstack/linkedin_api_start
{datetime.datetime.now()}
  '''.strip()  #TODO how to create hyperlink in a linkedin post? note markdown NOT possible

  reqbody = {
    'author': urn_author,

    'lifecycleState': 'PUBLISHED',
    'specificContent': {
      'com.linkedin.ugc.ShareContent': {
        'shareCommentary': {
          'text': post_content,
        },
        'shareMediaCategory': 'NONE'
      },
    },
    'visibility': {
      'com.linkedin.ugc.MemberNetworkVisibility': 'CONNECTIONS'  # value option here CONNECTIONS or PUBLIC ref https://learn.microsoft.com/en-us/linkedin/consumer/integrations/self-serve/share-on-linkedin#request-body-schema
    },
  }
  
  res = requests.post(
    'https://api.linkedin.com/v2/ugcPosts',
    #     ://       api_url_base/ugcPosts
    headers = {
      'Content-Type': 'application/json',
      'Authorization': f'Bearer {LINKEDIN_API__ACCESS_TOKEN}',
    },
    json = reqbody,
  )

  return res
  # {'id': 'urn:li:share:7105377103721746432'}
  # https://www.linkedin.com/feed/update/urn:li:share:7105377103721746432

  # {"id":"urn:li:share:7105369807541215232"}
  # can use this returned :id to open the post at https://www.linkedin.com/feed/update/urn:li:share:7105369807541215232/
  #                                                    ://                             urn:li:share:7105369807541215232
  #
  # manually open linkedin feeds and can see this post
  # at this link https://www.linkedin.com/feed/update/urn:li:activity:7105369807935459328?updateEntityUrn=urn%3Ali%3Afs_updateV2%3A%28urn%3Ali%3Aactivity%3A7105369807935459328%2CFEED_DETAIL%2CEMPTY%2CDEFAULT%2Cfalse%29&lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base%3B%2FZqHVBWAT9ShK8LzWZ0cEw%3D%3D
  # -> link of the created post https://www.linkedin.com/feed/update/urn:li:activity:7105369807935459328
  #                                  ://                             urn:li:activity:7105369807935459328
